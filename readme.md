# Zerbus WebComponents Components

Collection of WebComponents

## Import

Import the package in your `head` tag :
```!html
<script src='https://unpkg.com/zerbus@0.0.8/dist/zerbus.js'></script>
```

## Usage

### Zerbus datepicker

Add the datepicker anywhere :

```!html
<zerbus-datepicker></zerbus-datepicker>
```

Then display the datepicker by calling its `show()` method.

#### Input properties

| Name                  | Type                            | Default       | Description                                                                                                                 |
|-----------------------|---------------------------------|---------------|-----------------------------------------------------------------------------------------------------------------------------|
| enableBackdropDismiss | boolean                         | true          | Enable to dismiss the datepicker of the user click outside                                                                  |
| minDate               | Moment                          | null          | Minimum date, before which all dates are disabled                                                                           |
| maxDate               | Moment                          | null          | Maximum date, before which all dates are disabled                                                                           |
| disabledDates         | Moment[]                        | []            | Specific disabled dates                                                                                                     |
| customClasses         | {date: Moment, class: string}[] | []            | Class to apply for specific dates cell                                                                                      |
| customStyles          | {date: Moment, style: any}[]    | []            | Css style to apply for specific dates cell                                                                                  |
| language              | string                          | en            | Moment.js name of the language to use                                                                                       |
| positionOffset        | {x: number, y: number}          | {x: 30, y: 0} | The datepicker try to display on the better place in the window, if default value is not perfect, use this prop to adapt it |
#### Events

| Name           | Type                              | description                                    |
|----------------|-----------------------------------|------------------------------------------------|
| onSelected     | {date: Moment, event: MouseEvent} | Fired when the user click on an enabled date |
| onHover        | {date: Moment, event: MouseEvent} | Fired when a cell is hover                     |
| onLeave        | {date: Moment, event: MouseEvent} | Fired when a cell is leaved                    |
| onFocusChanged | Moment                            | Fired when the user select an other month    |

#### Methods

| Method    | Arguments | Return type | Description                   |
|-----------|-----------|-------------|-------------------------------|
| show      | null      | void        | Display the datepicker        |
| dismiss   | null      | void        | Dismiss the datepicker        |
| setLocale | string    | void        | Change the Moment.js language |
