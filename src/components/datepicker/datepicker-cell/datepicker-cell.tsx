import { Component, Event, EventEmitter, Prop } from '@stencil/core'

import moment from 'moment'

@Component({
  tag: 'zerbus-datepicker-cell',
  styleUrl: 'datepicker-cell.scss'
})
export class DatepickerCell {
  @Prop() day: moment.Moment
  @Prop() selectedDate: moment.Moment
  @Prop() focusedDate: moment.Moment
  @Prop() isDisabled: boolean
  @Prop() customClass: string
  @Prop() customStyle: any
  @Event() calendarCellClicked: EventEmitter<{date: moment.Moment, event: MouseEvent}>
  @Event() calendarCellHover: EventEmitter<{date: moment.Moment, event: MouseEvent}>
  @Event() calendarCellLeave: EventEmitter<{date: moment.Moment, event: MouseEvent}>

  get dayConst() {
    return {
      'day-content': true,
      today: this.day.isSame(moment(), 'day'),
      selected: this.day.isSame(this.selectedDate, 'day'),
      disabled: this.isDisabled
    }
  }

  render() {
    const cellClass = this.customClass ? `day ${this.customClass}` : 'day'
    const cellStyle = this.customStyle ? this.customStyle : {}

    return <td class={cellClass}
               style={cellStyle}
               onClick={($event) => {if (!this.isDisabled) this.calendarCellClicked.emit({date: this.day, event: $event})}}
               onMouseOver={($event) => {if (!this.isDisabled) this.calendarCellHover.emit({date: this.day, event: $event})}}
               onMouseLeave={($event) => {if (!this.isDisabled) this.calendarCellLeave.emit({date: this.day, event: $event})}}>
      <div class={this.dayConst}>
        <div>
          {this.day.isSame(this.focusedDate, 'month') ? this.day.format('D') : ''}
        </div>
      </div>
    </td>
  }
}
