import { Component, State } from '@stencil/core'

@Component({
  tag: 'datepicker-button',
  styleUrl: 'datepicker-button.scss'
})
export class DatepickerButton {
  @State() buttonClass: string = ''
  render() {
    return <button class={ this.buttonClass }
                   onFocus={() => this.onFocus()}>
      <slot/>
    </button>
  }

  onFocus(): void {
    this.buttonClass = 'activated'
    setTimeout(() => this.buttonClass = '', 200)
  }
}
