import { Component, Event, EventEmitter, Listen, Method, Prop, State } from '@stencil/core'

import moment from 'moment/min/moment-with-locales.min'

import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'

export interface Coord {
  x: number
  y: number
}

export interface CustomClass {
  date: moment.Moment,
  class: string
}

export interface CustomStyle {
  date: moment.Moment,
  style: any
}

@Component({
  tag: 'zerbus-datepicker',
  styleUrl: 'datepicker.scss'
})
export class CustomCalendar {

  @Prop() enableBackdropDismiss: boolean = true
  @Prop() minDate: moment.Moment = null
  @Prop() maxDate: moment.Moment = null
  @Prop() disabledDates: moment.Moment[] = []
  @Prop() customClasses: CustomClass[] = []
  @Prop() customStyles: CustomStyle[] = []
  @Prop() language: string = null
  @Prop() positionOffset: Coord = { x: -30, y: 0 }

  @Event() onSelected: EventEmitter<{date: moment.Moment, event: MouseEvent}>
  @Event() onHover: EventEmitter<{date: moment.Moment, event: MouseEvent}>
  @Event() onLeave: EventEmitter<{date: moment.Moment, event: MouseEvent}>
  @Event() onFocusChanged: EventEmitter<moment.Moment>

  @State() focusedDate: moment.Moment
  @State() selectedDate: moment.Moment = null
  @State() visible: boolean = false

  @State() position: Coord = { x: 0, y: 0 }

  constructor() {
    fontawesome.library.add(solid.faUser)
  }

  @Method()
  setLocale(language: string): void {
    moment.locale(language)
  }

  @Method()
  show($event: MouseEvent): void {
    this.position = {
      x: $event.clientX,
      y: $event.clientY
    }
    this.visible = true
  }

  @Method()
  dismiss() {
    this.visible = false
  }

  componentWillLoad() {
    if (this.language) moment.locale(this.language)
    this.focusedDate = moment()
  }

  get yearSelector() {
    return <thead class="datepicker-year-selector">
    <th colSpan={7}>
      <datepicker-button onClick={() => this.selectMonth(moment(this.focusedDate).add(-1, 'year'))}>
        <i class="fas fa-angle-left datepicker-icon"></i>
      </datepicker-button>

      <div class="year-label">{this.focusedDate.format('YYYY')}</div>
      <datepicker-button onClick={() => this.selectMonth(moment(this.focusedDate).add(1, 'year'))}>
        <i class="fas fa-angle-right datepicker-icon"></i>
      </datepicker-button>
    </th>
    </thead>
  }

  get monthSelector() {
    return <thead class="datepicker-month-selector">
    <th colSpan={7}>
      <datepicker-button class="large" onClick={() => this.selectMonth(moment(this.focusedDate).add(-1, 'month'))}>
        <i class="fas fa-angle-left datepicker-icon"></i>
      </datepicker-button>

      <div class="month-label">{this.focusedDate.format('MMMM')}</div>

      <datepicker-button class="large" onClick={() => this.selectMonth(moment(this.focusedDate).add(1, 'month'))}>
        <i class="fas fa-angle-right datepicker-icon"></i>
      </datepicker-button>
    </th>
    </thead>
  }

  private backdropClick() {
    if (this.enableBackdropDismiss) {
      this.dismiss()
    }
  }

  render() {

    let weeks = []
    for (let week: moment.Moment = moment(this.focusedDate).startOf('month').startOf('week');
         week.isSameOrBefore(this.focusedDate, 'month');
         week = moment(week).add(7, 'days')) {
      weeks.push(this.renderWeek(week))
    }

    let weeksHeader = []

    for (let day: moment.Moment = moment(this.focusedDate).startOf('week');
         day.isSame(this.focusedDate, 'week');
         day = moment(day).add(1, 'days')) {
      weeksHeader.push(<th class="day">{day.format('dd')[0]}</th>)
    }

    const tableStyle: any = {
      position: 'absolute',
      top: `${this.position.y + this.positionOffset.x}px`,
      left: `${Math.min(this.position.x + this.positionOffset.y, window.innerWidth - 285)}px`
    }

    return [
      <div class="backdrop" onClick={this.backdropClick.bind(this)} hidden={!this.visible}></div>,
      <table class="datepicker-month-view" hidden={!this.visible} style={tableStyle}>
        {this.yearSelector}
        {this.monthSelector}
        <thead class="week-header">
        <tr>{weeksHeader}</tr>
        <tr>
          <th class="table-divider" colSpan={7}></th>
        </tr>
        </thead>

        <thead class="month-header">
        <th>{this.focusedDate.format('MMM')}</th>
        </thead>
        {weeks}
      </table>]
  }

  renderWeek(week: moment.Moment) {
    let days = []
    for (let day: moment.Moment = moment(week).startOf('week');
         day.isSame(week, 'week');
         day = moment(day).add(1, 'days')) {
      days.push(this.renderDay(moment(day)))
    }
    return <tr class="week">
      {days}
    </tr>
  }

  renderDay(day: moment.Moment) {
    const localDay: moment.Moment = moment(day)
    let isDisabled: boolean = localDay.isBefore(this.minDate) || localDay.isAfter(this.maxDate) || !localDay.isSame(this.focusedDate, 'month') ? true : false
    this.disabledDates.some((disabledDate: moment.Moment) => {
      if (disabledDate.isSame(localDay, 'day')) {
        isDisabled = true
        return true
      }
    })
    let customClass: string = null
    this.customClasses.some((c: CustomClass) => {
      if (c.date.isSame(localDay, 'day')) {
        customClass = c.class
        return true
      }
    })
    let customStyle: any = null
    this.customStyles.some((c: CustomStyle) => {
      if (c.date.isSame(localDay, 'day')) {
        customStyle = c.style
        return true
      }
    })

    return <zerbus-datepicker-cell day={localDay}
                                 focusedDate={this.focusedDate}
                                 selectedDate={this.selectedDate}
                                 isDisabled={isDisabled}
                                 customClass={customClass}
                                 customStyle={customStyle}>
    </zerbus-datepicker-cell>
  }


  @Listen('calendarCellClicked')
  selectDay(event: CustomEvent): void {
    this.selectedDate = moment(event.detail.date)
    this.onSelected.emit({date: moment(this.selectedDate), event: event.detail.event})
    setTimeout(() => this.dismiss(), 100)

  }

  @Listen('calendarCellHover')
  dayHover(event: CustomEvent): void {
    this.onHover.emit(event.detail)
  }

  @Listen('calendarCellLeave')
  dayLeave(event: CustomEvent): void {
    this.onLeave.emit(event.detail)
  }

  selectMonth(day: moment.Moment): void {
    this.focusedDate = moment(day)
    this.onFocusChanged.emit(this.focusedDate)
  }
}
