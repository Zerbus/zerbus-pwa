import { Component, Listen } from '@stencil/core'

import moment from 'moment'

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.scss'
})
export class AppHome {

  render() {
    const minDate: moment.Moment = moment().subtract(4, 'months')
    const maxDate: moment.Moment = moment().add(4, 'months')

    const disabledDates: moment.Moment[] = [moment().add(-1, 'months'), moment().add(1, 'months'), moment().add(-2, 'days')]
    const customClasses = [{date: moment().add(-1, 'day'), class: 'testest'}]
    const customStyles = [{date: moment().add(3, 'day'), style: {color: 'orange'}}]
    return (
      <ion-page class='show-page'>
        <ion-header md-height='56px'>
          <ion-toolbar color='primary'>
            <ion-title>Ionic PWA Toolkit</ion-title>
          </ion-toolbar>
        </ion-header>

        <ion-content>
          <p>
            Welcome to the Ionic PWA Toolkit.
            You can use this starter to build entire PWAs all with
            web components using Stencil and ionic/core! Check out the readme for everything that comes in this starter out of the box and
            Check out our docs on <a href='https://stenciljs.com'>stenciljs.com</a> to get started.
          </p>

          <stencil-route-link url='/profile/stencil'>
            <ion-button>
              Profile page
            </ion-button>
          </stencil-route-link>

          <ion-button onClick={($event) => this.presentPopover($event)}>Show calendar</ion-button>

          <zerbus-datepicker minDate={minDate} maxDate={maxDate} disabledDates={disabledDates} customClasses={customClasses} customStyles={customStyles} language="fr"></zerbus-datepicker>
        </ion-content>
      </ion-page>
    );
  }

  presentPopover($event) {
    console.log($event)
    const calendar = document.querySelector('zerbus-datepicker');
    calendar.show($event);
  }

  @Listen('onSelected')
  daySelected(event: CustomEvent): void {
    console.log('selected', event.detail)
  }

  @Listen('onHover')
  dayHover(event: CustomEvent): void {
    console.log('focused', event.detail)
  }

  @Listen('onLeave')
  dayLeave(event: CustomEvent): void {
    console.log('leave', event.detail)
  }
}
