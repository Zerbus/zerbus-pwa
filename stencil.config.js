exports.config = {
  namespace: 'zerbus',
  hashFileNames: true,
  generateDistribution: true,
  generateWWW: false,
  bundles: [
    {components: ['zerbus-datepicker']}
  ],
  collections: [
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
